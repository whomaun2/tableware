package org.example.DAO;

import org.example.Entity.Product;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class DaoTableWare {
    public List<Product> csvFilePath = new ArrayList<>();

    public DaoTableWare() {
    }

    public List<Product> getAllProducts(String reader) {
        try (BufferedReader bufferedReader = Files.newBufferedReader(Path.of((reader)))) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(",");
                int id = Integer.parseInt(data[0]);
                String name = data[1];
                String category = data[2];
                int quantity = Integer.parseInt(data[3]);
                double price = Double.parseDouble(data[4]);

                Product product = new Product(id, name, category, quantity, price);
                csvFilePath.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
