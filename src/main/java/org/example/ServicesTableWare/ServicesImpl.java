package org.example.ServicesTableWare;

import org.example.DAO.DaoTableWare;
import org.example.Entity.Product;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;

public class ServicesImpl implements Services {
    public String csv = "src/main/resources/Products.csv";
    public Reader reader = new FileReader(csv);
    private DaoTableWare dao;
    List<Product>  data;

    public ServicesImpl() throws FileNotFoundException {
        this.dao = new DaoTableWare();
        this.data = dao.csvFilePath;
    }

    @Override
    public List<Product> getAllProducts() {
        dao.getAllProducts(csv);
        for (Product products : data) {
            System.out.println(products);
        }
        return null;
    }

    @Override
    public Product SearchingByName(String name) {
        dao.getAllProducts(csv);
        for (Product product : data) {
            if (product.getName().equalsIgnoreCase(name)) {
                System.out.println(product);
            }
        }

        return null;
    }

    @Override
    public Product SearchingByCategory(String category) {
        dao.getAllProducts(csv);
        for (Product product : data) {
            if (product.getCategory().equalsIgnoreCase(category)) {
                System.out.println(product);
            }
        }

        return null;
    }

    @Override
    public Product SearchingByQuantity(int quantity) {
        dao.getAllProducts(csv);
        for (Product product : data) {
            if (product.getQuantity() == quantity) {
                System.out.println(product);
            }
        }

        return null;
    }

    @Override
    public Product SearchingByPrice(double price) {
        dao.getAllProducts(csv);

        for (Product product : data) {
            if (product.getPrice() == price) {
                System.out.println(product);
            }
        }

        return null;
    }
}
