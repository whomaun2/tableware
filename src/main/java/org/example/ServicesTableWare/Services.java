package org.example.ServicesTableWare;

import org.example.Entity.Product;

import java.util.List;

public interface Services {
    List<Product> getAllProducts();
    Product SearchingByName(String name);
    Product SearchingByCategory(String category);
    Product SearchingByQuantity(int quantity);
    Product SearchingByPrice(double price);
}
