package org.example.Controller;

import org.example.ServicesTableWare.ServicesImpl;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class ControllerImpl implements Controller{
    public ServicesImpl services = new ServicesImpl();
    Scanner scanner = new Scanner(System.in);
    public ControllerImpl() throws FileNotFoundException {
    }
    @Override
    public void mainController() {
        boolean isRunning = true;
        while (isRunning) {
            System.out.println("1. Get all products");
            System.out.println("2. Search product by name");
            System.out.println("3. Search product by category");
            System.out.println("4. Search product by quantity");
            System.out.println("5. Search product by price");
            System.out.println("0. Exit");

            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    getAll(scanner);
                    break;
                case 2:
                    getName(scanner);
                    break;
                case 3:
                    getCategory(scanner);
                    break;
                case 4:
                    getQuantity(scanner);
                    break;
                case 5:
                    getPrice(scanner);
                    break;
                case 0:
                    isRunning = false;
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }
        }

    }

    @Override
    public void getAll(Scanner scanner) {
        services.getAllProducts();
    }

    @Override
    public void getName(Scanner scanner) {
        System.out.print("Enter product name: ");
        String name = scanner.nextLine();
        services.SearchingByName(name);
    }

    @Override
    public void getCategory(Scanner scanner) {
        System.out.print("Enter product category: ");
        String category = scanner.nextLine();
        services.SearchingByCategory(category);
    }

    @Override
    public void getQuantity(Scanner scanner) {
        System.out.print("Enter product quantity: ");
        int quantity = scanner.nextInt();
        scanner.nextLine();
        services.SearchingByQuantity(quantity);
    }

    @Override
    public void getPrice(Scanner scanner) {
        System.out.print("Enter product price: ");
        double price = scanner.nextDouble();
        scanner.nextLine();
        services.SearchingByPrice(price);
    }
}
