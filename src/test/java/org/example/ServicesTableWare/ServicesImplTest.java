package org.example.ServicesTableWare;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

class ServicesImplTest {

    public ServicesImpl services;

    @BeforeEach
    void setUp() throws FileNotFoundException {
        services = new ServicesImpl();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testGetAllProducts() {
        services.getAllProducts();
    }

    @Test
    void testSearchingByName() {
        String name = "Dish Set";
        services.SearchingByName(name);
    }

    @Test
    void testSearchingByCategory() {
        String category = "Tableware";
        services.SearchingByCategory(category);
    }

    @Test
    void testSearchingByQuantity() {
        int quantity = 10;
        services.SearchingByQuantity(quantity);
    }

    @Test
    void testSearchingByPrice() {
        double price = 29.0;
        services.SearchingByPrice(price);
    }
}
