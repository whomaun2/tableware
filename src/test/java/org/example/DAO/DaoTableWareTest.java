package org.example.DAO;

import org.example.Entity.Product;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DaoTableWareTest {
    private DaoTableWare dao;
    private static final String CSV_FILE_PATH = "src/main/resources/Products.csv";

    @BeforeEach
    void setUp() {
        dao = new DaoTableWare();
    }

    @AfterEach
    void tearDown() {
        dao = null;
    }

    @Test
    void testGetAllProducts() {
        try {
            dao.getAllProducts(CSV_FILE_PATH);
            List<Product> products = dao.csvFilePath;
            assertEquals(20, products.size());
            Product product1 = products.get(0);
            assertEquals(1, product1.getId());
            assertEquals("Dinnerware Set", product1.getName());
            assertEquals("Tableware", product1.getCategory());
            assertEquals(10, product1.getQuantity());
            assertEquals(29.0, product1.getPrice(), 0.01);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        // Add more assertions for other products
    }
}
